
var checker = 0;

$(document).ready
(
function()
{		
	$('#getNodesButton').click
	(function()
	{
	    var staff = 'staff';
		$.post("get.php", {'staff': staff},
		function(nodes) //Timetable is dynamically filled
		{
			var nodeData = nodes;
			//alert(JSON.stringify(nodeData, null, 4));
			var nodeFiller = {};
			nodeFiller['core'] = nodeData['core'];
			nodeFiller['dist'] = nodeData['dist'];
			nodeFiller['access'] = nodeData['access'];
			var stringCreate = nodeData['hosts'];
			var arrayCreate = stringCreate.split(",");
			nodeFiller['hosts'] = arrayCreate;
			$('#nodeDiv').html(''); //ensures multiple copies of the data isn't put into this div

			for (var stuff in nodeFiller)
			{
			  if (stuff == 'core')
			  {     var coreID = 0
				for (b=0;b<parseInt(nodeFiller[stuff]);b++)//repeat for each node
				{
				$('<div id="core'+coreID+'" class="inter">core</div>').appendTo($('#nodeDiv'));
				coreID++;
				}
			  }

			  if (stuff == 'dist')
			  {	var distID = 0;
				for (b=0;b<parseInt(nodeFiller[stuff]);b++)//repeat for each node
				{
				$('<div id="dist'+distID+'" class="inter">dist</div>').appendTo($('#nodeDiv'));
				distID++;
				};
			  }

			  if (stuff == 'access')
			  {	var accID = 0;
				for (b=0;b<parseInt(nodeFiller[stuff]);b++)//repeat for each node
				{
				$('<div id="access'+accID+'" class="inter">access</div>').appendTo($('#nodeDiv'));
				accID++;
				};
			  }

			  if (stuff == 'hosts')
			  {	var hostID = 0;
				for (x=0;x<nodeFiller[stuff].length;x++)
				{
				$('<div id="host'+hostID+'" class="end">'+nodeFiller[stuff][x]+'</div>').appendTo($('#nodeDiv'));
				hostID++;
				}
			  }
			}
			$('.inter').draggable({containment:"#nodeDiv"}).css({border:'solid thin black', padding:'0.5em',margin:'0.2em', width:'4em', borderRadius:'0.3em', float:'left'}).mouseover().css({cursor:'pointer'});
			$('.end').draggable({containment:"#nodeDiv"}).css({border:'solid thin black', padding:'0.5em', margin:'0.2em', width:'4em', borderRadius:'0.3em', float:'left'}).mouseover().css({cursor:'pointer'});
			$('#nodeDiv').append('<div id="optiDiv">Layer 2 Optimisation <br>(drag hosts in here)<div></div></div>');
		},"json"
		);
	}
	);

	$('#clear').click
	(function()
	{
	  var clearer = '';
	  $.post("clear.php", {'value': clearer});
	}
	);
	
	$('#apply').click
	(function()
	{
		var optiOff = $('#optiDiv').offset();
		var maxTop = optiOff.top + 100;
		var maxLeft = optiOff.left + 150;
		var holder = [];
		optiHosts = $('.end');

		for (c=0;c<optiHosts.length;c++)
		{ //alert($(optiHosts[c]).offset().top);alert($(optiHosts[c]).offset().left);
		  if((optiOff.top < $(optiHosts[c]).offset().top && $(optiHosts[c]).offset().top < maxTop) && (optiOff.left < $(optiHosts[c]).offset().left && $(optiHosts[c]).offset().left < maxLeft))
		  {holder.push(optiHosts[c]);}
		}
		var answer = "";

		for (d=0;d<holder.length;d++)
		{
		  answer += $(holder[d]).text();
		  answer += ":"

		}
		$.post("apply.php", {'value': answer});
	}
	);
}
);