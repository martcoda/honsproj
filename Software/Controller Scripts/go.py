#! /usr/bin/python
import os
import time

print "Do you want DEBUG logging level or CRITICAL? Enter 'd' or 'c'"
verbosity = raw_input("> ") # input desired verbosity of error messages as a string
if verbosity.lower() == "c" or verbosity.lower() == "critical": # if capital C in put, change it to lower, if lower c input, fine
  answer = "CRITICAL"
  print "Ok you have chosen CRITICAL"
elif verbosity.lower() == "d" or verbosity.lower() == "debug":
  answer = "DEBUG"
  print "Ok you have chosen DEBUG"
else:
  print "I didn't quite catch that, am assuming that you will accept DEBUG"
  answer = "DEBUG"

print "Do you want to use spanning_tree or have you entered blockages manually?"
print "Enter 's' if you want to use spanning_tree, otherwise enter 'm'"
determ = raw_input("> ")
if determ.lower() == "m":
  print "Ok you have chosen manual blockages"
  detery = "deterministic"
  command = "/home/mininet/pox/pox.py log.level --%s forwarding.l2_learning HonsProj"%(answer)
elif determ.lower() == "s":
  print "Ok you have chosen to use the POX version of STP"
  detery = "non"
  command = "/home/mininet/pox/pox.py log.level --%s samples.spanning_tree HonsProj"%(answer)
else:
  print "I didn't quite catch that... continuing with NON-DETERMINISTIC (the default)"
  detery = "non"
  command = "/home/mininet/pox/pox.py log.level --%s samples.spanning_tree HonsProj"%(answer)
  
toOpen = "/home/mininet/pox/topoType"
writer = open(toOpen,'w') # open file...
writer.write("%s" %(detery))# write the nodes to the file
writer.close() # closing writer object

print "Please wait a moment while the controller starts....."
time.sleep(8) # wait 8 seconds to give Mininet simulated network a chance to load :)
os.system(command) # finally start the controller with specified verbosity and topology type

# learned how to do time from ubuntuforums.org/showthread.php?t=715907
# learned how to do os.system from stackoverflow.com/questions/3781851/run-a-python-script-from-another-python-script-passing-in-args