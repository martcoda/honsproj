from pox.core import core
import pox.openflow.libopenflow_01 as of
import threading
import thread
import time
from pox.lib.recoco import Timer
import sys as sys #hopefully can use for exiting out of event handler which increments each time

import struct
import operator
import collections
from itertools import chain, repeat
import sys
from pox.lib.packet.packet_base import packet_base
from pox.lib.packet.ethernet import ethernet
from pox.lib.packet.vlan import vlan
from pox.lib.packet.ipv4 import ipv4
from pox.lib.packet.udp import udp
from pox.lib.packet.tcp import tcp
from pox.lib.packet.icmp import icmp
from pox.lib.packet.arp import arp

from pox.lib.addresses import *
from pox.lib.util import assert_type
from pox.lib.util import initHelper
from pox.lib.util import hexdump

from pox.openflow.discovery import Discovery
log = core.getLogger()

def getTheTime(): # function to create a timestamp

  flock = time.localtime() # thanks to http://desk.stinkpot.org:8080/tricks/index.php/2007/11/get-timestamp-in-python/
  then = "[%s-%s-%s" % (str(flock.tm_year),str(flock.tm_mon),str(flock.tm_mday))

#this bit is to give a nice format to the timestamp, i.e. 9 mins becomes 09 mins
  if int(flock.tm_hour) < 10:
    hrs = "0%s" % (str(flock.tm_hour))
  else:
    hrs = str(flock.tm_hour)
  if int(flock.tm_min) < 10:
    mins = "0%s" % (str(flock.tm_min))
  else:
    mins = str(flock.tm_min)
  if int(flock.tm_sec) < 10:
    secs = "0%s" % (str(flock.tm_sec))
  else:
    secs = str(flock.tm_sec)
  then += "]%s.%s.%s" % (hrs,mins,secs) # formatted time is assigned to a variable. Note if you use colons in a filename gedit hates it
  return then # return the complete formatted datetimestamp

def handle_flow_stats (event): # this function handles flow stats returned to the controller from switches

  # I need to state global before these variables and constants to allow them to be used and updated within this function
  global switchOrder # this is reset in the launch function every loop, used to hold the order of nodes in a flow path
  global switchCounter # this is used to count returned stats from switches, so I know when all switches have replied
  global mdf # list of core switch dpid's
  global idf # list of idf switch dpid's
  global hwc # list of hwc switch dpid's
  global normal # list of normal links
  global redundant # list of redundant links
  global holdFlows
  global readin
  global flowDuration
  global flowBytes
  global pathLength

  howmany = 0 # variable used for counting number of relevant flows in the returned stats
  for things in event.stats: # flow is relevant if id is not 333 (manually installed) or if duration longer than 10 seconds
    if things.duration_sec > flowDuration and things.cookie != 333:
      howmany += 1
      if things.byte_count > flowBytes: # check if byte count is large enough to add flow to holdFlows dictionary
        # And here I'm creating a table to see order of flow entries to find path
        flowDNA = "%s:%s>%s:%s"%(things.match.nw_src,things.match.tp_src, things.match.nw_dst, things.match.tp_dst)

        pathNode = event.connection.dpid # switch datapath id

        if pathNode in holdFlows.keys(): # holdFlows will be used for assigning match attributes for heavy flows
          holdFlows[pathNode].append([things]) # append stats object with switch ID as the key
        else:
          holdFlows[pathNode] = [[things]] # create key of switch ID with value being the stats object

# thanks to http://stackoverflow.com/questions/1602934/what-is-a-good-way-to-test-if-a-key-exists-in-python-dictionary
        if flowDNA in switchOrder.keys(): # if flowDNA already added to dictionary just append to that entry
          switchOrder[flowDNA].append([things.duration_nsec,pathNode])
        else: # Otherwise add it to the dictionary plus some values
          switchOrder[flowDNA] = [[things.duration_nsec,pathNode]]
        list.sort(switchOrder[flowDNA]) # values are sorted each time round
      else:# if byte count is not large enough, check if flow is a complement of one which is large enough, and if so store this one too
        reverseDNA = "%s:%s>%s:%s"%(things.match.nw_dst,things.match.tp_dst,things.match.nw_src,things.match.tp_src)
        if reverseDNA in switchOrder.keys(): # if the complement of this flow has been stored for later then add this one too
          flowDNA = "%s:%s>%s:%s"%(things.match.nw_src,things.match.tp_src, things.match.nw_dst, things.match.tp_dst)

          pathNode = event.connection.dpid # switch datapath id

          if pathNode in holdFlows.keys(): # holdFlows will be used for assigning match attributes for heavy flows
            holdFlows[pathNode].append([things]) # append stats object with switch ID as the key
          else:
            holdFlows[pathNode] = [[things]] # create key of switch ID with value being the stats object


# thanks to http://stackoverflow.com/questions/1602934/what-is-a-good-way-to-test-if-a-key-exists-in-python-dictionary
          if flowDNA in switchOrder.keys(): # if flowDNA already added to dictionary just append to that entry
            switchOrder[flowDNA].append([things.duration_nsec,pathNode])

          else: # Otherwise add it to the dictionary plus some values
            switchOrder[flowDNA] = [[things.duration_nsec,pathNode]]
          list.sort(switchOrder[flowDNA]) # values are sorted each time round        

 # Here a message is printed containing the number of relevant flows
  print "\n<<<STATS-REPLY Returned %s FLOW stats for Switch"%(str(howmany)), event.connection.dpid, "at", getTheTime()
  for f in event.stats:      # cycles through each flow's stats returned to the controller
  #print f.match.from_packet.ip_proto
  #print dir(f.match.from_packet)
    ipproto = {1:'ICMP',6:'TCP',17:'UDP'}
    flowid = int(f.cookie)                   # this identifies the rule matching the flow
    if f.duration_sec > flowDuration or flowid == 333:# if the flow has lasted longer than idle timeout

      if flowid == 333:
        print "\n    STP rule",flowid,"in place on this switch" # STP flows printed here
      else:
        if f.byte_count > flowBytes:
        # print details of the long-lasting flow
          bytes = int(f.byte_count)              # first change byte count into kB
          bytes = bytes/1024
   # Long-duration flows printed here
          try:
            print "\n    Flow lasting",f.duration_sec, "secs, ByteCount is %dkB"%(bytes),":\n      ", ipproto[f.match.nw_proto], f.match.nw_src, "> %s;"%(f.match.nw_dst), "Packets = %s;"%(f.packet_count), "Ports = %s"%( f.match.tp_src), "> %s;"%( f.match.tp_dst)
          except: pass
  # print "hello %d"%(switchCounter)
  switchCounter += 1 # this increments on each event as switchCounter is a global variable
  if switchCounter == len(core.openflow.connections): # if all switches have replied, print the dict
    print "All switches have replied..."

    if len(readin) < 1:
     print "NO REQUESTS FOR OPTIMISATION MADE THIS TIME ROUND"
    else:
      print "OPTIMISATIONS HAVE BEEN REQUESTED FOR FLOWS INVOLVING THESE HOSTS:\n", readin
      for k in switchOrder.keys(): # cycle through each flowDNA key
        checker = 0 # used to verify that flowDNA contains a hosts IP which has been requested for optimisation, needs to be reset for each flowDNA kehy
        for opter in readin:
          if str(opter) in str(k): # checking if requested host is in flowDNA
            checker += 1 # if host in flwoDNA increment checker

        if checker > 0: # if checker has been incremented then a relevant flow has been found for hosts to optimise, so continue...
          
          list.reverse(switchOrder[k]) # durations in reverse order of flow path

          size = len(switchOrder[k]) # number of nodes in flow path
          if size < pathLength: # pathLength was set to 4 as a global variable
            # check if flow path is less than 4 nodes, in which case don't optimise
            print "Path of flow %s is too short to optimise" % (k)
          else: # if flow path is 4 nodes or more, print it
            print "\nSwitches traversed by flow %s are:" % (k) # print flow
            for v in switchOrder[k]:

              if int(v[1]) in mdf: # check what kind of switch it is
                heir = "core"
              elif int(v[1]) in idf:
                heir = "distribution"
              elif int(v[1]) in hwc:
                heir = "access"

              print "%s %s (duration is %s ns)"%(heir, v[1], v[0]) # print path

            # get first and last switches in the path to allow for a little difference in duration
            #firstnsecondnthird = [int(switchOrder[k][0][1]),int(switchOrder[k][1][1])]
            accessHolder = []
            for s in switchOrder[k]: # check the first three nodes cos sometimes they're out of order
              if s[1] in hwc: # compare with list of access switches to pick out the access switch
                accessHolder.append([s[0],s[1]])
            list.sort(accessHolder) # this should put the longest duration at the end of the list
            lastAccess = accessHolder[0][1] # take short duration access switch
            firstAccess = accessHolder[1][1]# take longest duration access switch
			# get first and last switches in the path to allow for a little difference in duration
            #firstnsecondnthird = [int(switchOrder[k][0][1]),int(switchOrder[k][1][1]),int(switchOrder[k][2][1])]
            #for s in firstnsecondnthird: # check the first three nodes cos sometimes they're out of order
            #  if s in hwc: # compare with list of access switches to pick out the access switch
            #    firstAccess = s

            print "Start of path is %d" % (firstAccess) # print the first access switch in the path

            # get the secondlast and last switches in the path to allow for a little diff in durati
            # secondlastnlast = [int(switchOrder[k][size-2][1]),int(switchOrder[k][size-1][1])]
            # the above didn't work because sometimes I'd get an access switch 3 places out of sequence
            #for l in switchOrder[k]: # check all path nodes for another access switch
            #  if l[1] in hwc and l[1] != firstAccess: # ensure it is not the firstAccess
            #    lastAccess = l[1] # in which case it should be the last Access switch in the path

            print "End of path is %d" % (lastAccess) # print the last access switch in the path

            # discover which distribution switches the first and last have in common

            firstNormal = []
            firstRed = []
            lastNormal = []
            lastRed = []

            for x in normal: # find instances of this switch in the links normal list
              if int(x[0]) == int(firstAccess):
                firstNormal.append(x) # append to new list
              if int(x[2]) == int(firstAccess):
                firstNormal.append([x[2],x[3],x[0],x[1]]) # reverse it so they're all in the same order
      
            for y in redundant: # find instances of this switch in the links redundant list
              if int(y[0]) == int(firstAccess):
                firstRed.append(x) # append to new list
              if int(y[2]) == int(firstAccess):
                firstRed.append([y[2],y[3],y[0],y[1]]) # reverse it so they're all in the same order

            for x in normal: # find instances of this switch in the links normal list
              if int(x[0]) == int(lastAccess):
                lastNormal.append(x) # append to new list
              if int(x[2]) == int(lastAccess):
                lastNormal.append([x[2],x[3],x[0],x[1]]) # reverse it so they're all in the same order
      
            for x in redundant: # find instances of this switch in the links redundant list
              if int(x[0]) == int(lastAccess):
                lastRed.append(x) # append to new list
              if int(x[2]) == int(lastAccess):
                lastRed.append([x[2],x[3],x[0],x[1]]) # reverse it so they're all in the same order
      
          #print "\nStart switch",firstNormal, firstRed
          #print "\nEnd switch", lastNormal, lastRed

            firstNeighbours = [] # list for holding start switch neighbour id's
            lastNeighbours = [] # list for holding end switch neighbour id's

            # populate firstNeighbours list
            for x in firstNormal:
              firstNeighbours.append(x[2])
            for x in firstRed:
              firstNeighbours.append(x[2])

            # populate lastNeighbours list
            for x in lastNormal:
              lastNeighbours.append(x[2])
            for x in lastRed:
              lastNeighbours.append(x[2])

            commonNeighbours = [] # list to hold common neighbours
      
            for x in firstNeighbours:
              if x in lastNeighbours:
                commonNeighbours.append(x) # populate commonNeighbours list

            print "Start and End switches for this flow have these distribution switches in common", commonNeighbours
     
            srcIP = ""
            srcPort = ""
            dstIP = ""
            dstPort = ""
            countColons = 0
            countArrow = 0

            for c in k: # iterate through each character of flowDNA to pull out flow src & dst ip's and ports
              if str(c) == ":": # keep track of colons
                countColons += 1
              elif str(c) == ">": # keep track of arrow
                countArrow += 1
              elif countArrow == 0 and countColons == 0: # srcIP
                srcIP += c
              elif countArrow == 0 and countColons == 1: # srcPort
                srcPort += c
              elif countArrow == 1 and countColons == 1: # dstIP
                dstIP += c
              elif countArrow == 1 and countColons == 2: # dstPort
                dstPort += c
    
            for z in core.openflow.connections:

              if z.dpid == firstAccess: # select firstAccess connection
            
               for n in commonNeighbours: 
                validNeighbour = 0
                for y in switchOrder[k]: # cycle through path of this flow
                 if n == y[1]: # check if the common neighbour was already part of the flow path!
                  validNeighbour += 1
                if validNeighbour > 0: # if common neighbour was already part of the flow path, go ahead. 
                  print "OPTIMISING FLOW %s-%s--%s-%s"%(srcIP,srcPort,dstIP,dstPort)
                  fANeports = []
                  lANeports = []
                  norm = 0 # variable to check if link between lastAccess and common neighbour was normal or redundant

                  for j in redundant:
                    if j[0] == firstAccess and j[2] == n:
                      fANeports.append([j[1],j[3]])
                    elif j[2] == firstAccess and j[0] == n:
                      fANeports.append([j[3],j[1]])

                  for j in redundant: # checking for a single entry in redundant as there is only 1 link from the common neighbour
                    if j[0] == lastAccess and j[2] == n:
                      lANeports.append([j[1],j[3]])
                    elif j[2] == lastAccess and j[0] == n:
                      lANeports.append([j[3],j[1]])

                  for j in normal: # the single link may however be a normal link, so checking those too.....
                    if j[0] == lastAccess and j[2] == n:
                      lANeports.append([j[1],j[3]])
                      norm += 1
                    elif j[2] == lastAccess and j[0] == n:
                      lANeports.append([j[3],j[1]])
                      norm += 1

                  if len(fANeports) < 1:
                    print "No redundant links between firstAccess",z.dpid,"and neighbour",n
                  else:
                    if norm > 0:
                      linktype = "normal"
                    else:
                      linktype = "redundant"
                    print "The redundant link between firstAccess and this neighbour is",fANeports
                    print "The %s link between lastAccess and this neighbour is"%(linktype),lANeports
                    for p in fANeports:

                      print "Rule for first access",firstAccess,"is",srcIP,srcPort,">",dstIP, dstPort,"send out",p[0]
                      stpMatch = of.ofp_match()
                      #print "len of holfflows for this switch is",len(holdFlows[firstAccess])
                      for w in holdFlows[firstAccess]: # for each stats object holder for first access switch of path
                        #print "length of w is",len(w)
                        #print "dir of w[0] is",dir(w[0]) # select the stats object
                        #print "l3src",w[0].match.nw_src, "l3dst",w[0].match.nw_dst
                        if str(w[0].match.nw_src) == str(srcIP) and str(w[0].match.nw_dst) == str(dstIP) and str(w[0].match.tp_src) == str(srcPort) and str(w[0].match.tp_dst) == str(dstPort):
                          # if the socket matches, this is the correct flow so take the matching attributes from the stats object....
                          stpMatch.dl_src = w[0].match.dl_src # data link layer source address (MAC address)
                          stpMatch.dl_dst = w[0].match.dl_dst # data link layer destination address (MAC address)
                          stpMatch.dl_vlan = w[0].match.dl_vlan # data link layer VLAN
                          stpMatch.dl_vlan_pcp = w[0].match.dl_vlan_pcp
                          stpMatch.dl_type = w[0].match.dl_type # Layer 2 type...2048 is ipv4 (0x0800)
                          stpMatch.nw_tos = w[0].match.nw_tos
                          stpMatch.nw_proto = w[0].match.nw_proto # transport layer protocol, 1:'ICMP',6:'TCP',17:'UDP'
                          stpMatch.in_port = w[0].match.in_port # ingress port

                          # and now add the socket attributes
                          stpMatch.nw_src = "%s/32" % (srcIP) 
                          stpMatch.nw_dst = "%s/32" % (dstIP)

                          stpMatch.tp_src = int(srcPort)
                          stpMatch.tp_dst = int(dstPort)
                          stpRules = of.ofp_flow_mod()
                          stpRules.match = stpMatch
                          stpRules.cookie = 333 # this is a flow identifier :)
                          stpRules.idle_timeout  = 7
                          stpRules.hard_timeout = 10000
                          stpRules.priority = 65535
                          stpRules.actions.append(of.ofp_action_output(port = p[0]))
                          print "INSTALLING RULE INTO FIRST ACCESS"
                          z.send(stpRules)
					  # and now for reverse rule on same switch
                          stpMatch.dl_src = w[0].match.dl_dst
                          stpMatch.dl_dst = w[0].match.dl_src
                          stpMatch.in_port = p[0]
                          stpMatch.nw_src = dstIP
                          stpMatch.nw_dst = srcIP
                          stpMatch.tp_src = int(dstPort) #0
                          stpMatch.tp_dst = int(srcPort) #0
                          stpRules.match = stpMatch
                          stpRules.actions.append(of.ofp_action_output(port = w[0].match.in_port))
                          ahora = "/home/mininet/pox/changeable/rulesInTime="
                          ahora += str(getTheTime())
                          writer = open(ahora,'w') # create file...
                          writer.write(getTheTime()) # write the current time to the opened file
                          writer.close() # closing writer object
                          print "INSTALLING REVERSE RULE INTO FIRST ACCESS at",getTheTime() # this timestamp is used to match up with hping times
                          z.send(stpRules)
					  
                          print "Path optimisation rules installed into switch",z.dpid

                          print "Reverse rules for neighbour",n,"is",dstIP,dstPort,">",srcIP,srcPort,"send out",p[1]
                          stpMatch = of.ofp_match()
                          #print "len of holfflows for this switch is",len(holdFlows[lastAccess])
                     
                          stpMatch.dl_src = w[0].match.dl_dst
                          stpMatch.dl_dst = w[0].match.dl_src
                          stpMatch.dl_vlan = w[0].match.dl_vlan
                          stpMatch.dl_vlan_pcp = w[0].match.dl_vlan_pcp
                          stpMatch.dl_type = w[0].match.dl_type # 2048 is ipv4
                          stpMatch.nw_tos = w[0].match.nw_tos
                          stpMatch.nw_proto = w[0].match.nw_proto
                          stpMatch.in_port = lANeports[0][1] # port of neighbour which receives the flow from lastAccess
    
                          stpMatch.nw_src = "%s/32" % (dstIP)
                          stpMatch.nw_dst = "%s/32" % (srcIP)
                          stpMatch.tp_src = int(dstPort)
                          stpMatch.tp_dst = int(srcPort)
                          stpRules = of.ofp_flow_mod()
                          stpRules.match = stpMatch
                          stpRules.cookie = 333 # this is a flow identifier :)
                          stpRules.idle_timeout  = 7
                          stpRules.hard_timeout = 10000
                          stpRules.priority = 65535
                          stpRules.actions.append(of.ofp_action_output(port = p[1]))
                          # end of first rule for neighbour
                          # start of second rule for neighbour
                          neighbourMatch = of.ofp_match()
                          neighbourMatch.dl_vlan = w[0].match.dl_vlan
                          neighbourMatch.dl_vlan_pcp = w[0].match.dl_vlan_pcp
                          neighbourMatch.dl_type = w[0].match.dl_type # 2048 is ipv4
                          neighbourMatch.nw_tos = w[0].match.nw_tos
                          neighbourMatch.nw_proto = w[0].match.nw_proto
                          neighbourMatch.dl_src = w[0].match.dl_src
                          neighbourMatch.dl_dst = w[0].match.dl_dst
                          neighbourMatch.in_port = p[1]
                          neighbourMatch.nw_src = "%s/32" % (srcIP)
                          neighbourMatch.nw_dst = "%s/32" % (dstIP)
                          neighbourMatch.tp_src = int(srcPort)
                          neighbourMatch.tp_dst = int(dstPort)
                          neighbourRules = of.ofp_flow_mod()
                          neighbourRules.match = neighbourMatch
                          neighbourRules.cookie = 333 # this is a flow identifier :)
                          neighbourRules.idle_timeout  = 7
                          neighbourRules.hard_timeout = 10000
                          neighbourRules.priority = 65535
                          neighbourRules.actions.append(of.ofp_action_output(port = lANeports[0][1]))

                          for q in core.openflow.connections:
                            if q.dpid == n:
                              print "INSTALLING BOTH RULES INTO COMMON NEIGHBOUR %s"%(n)
                              q.send(stpRules)
                              q.send(neighbourRules)
                        else:
                          pass
                else: # this is for if the common neighbour is not already part of the flow path, in which case don't optimise!
                  print "OPTIMISATION AVOIDED FOR UNSUITABLE NEIGHBOUR %s"%(n) 
        else:
          pass  # this is the else of the if flowdna contains ip of a host to be optimised               
  else: # this is for if all switches have replied
    pass


def handle_port_stats (event):#this function handles port stats returned to the controller from switches

#this bit is to give a nice format to the timestamp, i.e. 9 mins becomes 09 mins

  print "\n<<<STATS-REPLY: Returned PORT stats for Switch", event.connection.dpid, "at",getTheTime()
  #for z in event.connection.ports.keys():
    #print "key",z,"value",event.connection.ports.get(z)
  for f in event.stats:
    print "    Port Number:",f.port_no, ". Fwd'd Packets:", f.tx_packets, ". Rc'd Packets:",f.rx_packets
    #print dir(f)

def handle_features_reply (event):#this function handles features stats returned to the controller from switches

    global normal
    global redundant
    global interimNormal
    global interimRedundant
    global switchCounter2

    for m in event.connection.features.ports:
      if m.config == 0: # if there is no particular config in place. i.e this is a normal link, add it to normal list
        for edge in core.openflow_discovery.adjacency: 
          if edge.dpid1 == event.connection.dpid and edge.port1 == m.port_no: # if the first dpid of the adjacency is this switch, then proceed....
            appendable = 0
            if len(interimNormal) > 0: # check if normal has any entries yet, if so check for existing entry clashes.....
              for n in interimNormal: # iterate through normal checking to see if entry already exists
                if n[0] == event.connection.dpid and n[1] == m.port_no: # if already exists as a source
                  appendable += 1
                elif n[2] == event.connection.dpid and n[3] == m.port_no: # if already exists as a destination
                  appendable += 1
              if appendable < 1:
                interimNormal.append([event.connection.dpid, m.port_no, edge.dpid2, edge.port2])
            else: # if normal hasn't been populated yet, go ahead and start populating it
              interimNormal.append([event.connection.dpid, m.port_no, edge.dpid2, edge.port2])
			  
      if m.config == 16: # if the OFPPC_NO_FLOOD config is in place, this is a redundant link, add it to redundant list
        for edge in core.openflow_discovery.adjacency:
          if edge.dpid1 == event.connection.dpid and edge.port1 == m.port_no: # if the first dpid of the adjacency is this switch, then proceed....
            appendable = 0
            if len(interimRedundant) > 0:
              for r in interimRedundant: # iterate through redundant checking to see if entry already exists
                if r[0] == event.connection.dpid and r[1] == m.port_no: # if already exists as a source
                  appendable += 1
                if r[2] == event.connection.dpid and r[3] == m.port_no: # if already exists as a destination
                  appendable += 1
              if appendable < 1:
                interimRedundant.append([event.connection.dpid, m.port_no, edge.dpid2, edge.port2])
            else:
              interimRedundant.append([event.connection.dpid, m.port_no, edge.dpid2, edge.port2]) 
			  
    switchCounter2 += 1
    if len(core.openflow.connections) == switchCounter2:
      list.sort(interimNormal) # finally pass stable lists to the lists which algorithm uses
      list.sort(interimRedundant) # finally pass stable lists to the lists which algorithm uses
      normal = interimNormal
      redundant = interimRedundant
      print "\nNormal links are", normal
      print "Redundant links are", redundant
    else: pass
	
def detFirstThingsFirst ():

  core.openflow.addListenerByName("FlowStatsReceived", handle_flow_stats)
  core.openflow.addListenerByName("PortStatsReceived", handle_port_stats)
  
def nondetFirstThingsFirst ():

  core.openflow.addListenerByName("FlowStatsReceived", handle_flow_stats)
  core.openflow.addListenerByName("PortStatsReceived", handle_port_stats)
  core.openflow.addListenerByName("FeaturesReceived", handle_features_reply)
  
def stpLaunch ():

  global redundant
  for connection in core.openflow.connections:
      redundantBlocker = [] # list of ports to be blocked
      for r in redundant: 
        if r[0] == connection.dpid:
          redundantBlocker.append(r[1])
        if r[2] == connection.dpid:
          redundantBlocker.append(r[3])
		  
      print "installing blockages for switch",connection.dpid
      print "blocking",redundantBlocker
      for r in redundantBlocker: # cycle through ports to be blocked
        for x in connection.features.ports: # cycle through available ports for this switch
          if x.port_no == r: # if port is one in the list of ports to block
            theAnswer = x.hw_addr # get the MAC address of this port

        stpPortRules = of.ofp_port_mod() # create a port_mod
        stpPortRules.port_no = r # identify port by number
        stpPortRules.hw_addr = theAnswer  # identify port by MAC
        stpPortRules.config =  of.OFPPC_NO_FLOOD # of.OFPPC_PORT_DOWN this is the config to be applied
        stpPortRules.mask = of.OFPPC_NO_FLOOD #of.OFPPC_PORT_DOWN # this is the config to be applied
        connection.send(stpPortRules) # send this port mod to the switch

  print "\nOk STP blockages installed into switches"

def launch ():
  """
  Starts periodic request for stats
  """
  global deter
  global holdFlows
  global switchCounter # used to ensure all switches have replied before printing switchOrder
  global switchCounter2 # used in features request to ensure all switches have replied before printing
  global switchOrder # a dictionary containing flowpath data
  global readin
  global normal # list for normal links
  global redundant # list for redundant links
  global interimNormal
  global interimRedundant

  switchCounter = 0 # empty for this round of flow stat results
  switchCounter2 = 0
  switchOrder = {} # empty ready for this round of flow stat results
  holdFlows = {} # empty ready for this round of flow stat results
  
  interimNormal = []
  interimRedundant = []

  readin = []
  toRead = "/home/mininet/pox/changeable/interact"
  reader = open(toRead,'r') # open file...
  for line in reader.readlines():
    appender = ""
    for c in line:
      if c != ":":
        appender += c
      if c == ":":
        readin.append(appender)
        appender = ""
  reader.close()

  if len(core.openflow.connections) > 0:
    print "\n>>>>>>STATS-REQUESTS FOR ALL SWITCHES: "

  for connection in core.openflow.connections:
      #print "\n>>>>>>>>>>>>>>>>>>>>>>>>>: Requesting stats for switch",connection.dpid
      connection.send(of.ofp_features_request()) # works
      connection.send(of.ofp_stats_request(body=of.ofp_flow_stats_request()))#works

      #connection.send(of.ofp_stats_request(body=of.ofp_port_stats_request()))#works
	  
      #connection.send(of.ofp_stats_request(body=of.ofp_desc_stats_request()))
      #connection.send(of.ofp_get_config_request())	  
      #connection.send(of.ofp_desc_stats_request())
      #connection.send(of.ofp_stats_request(body=of.ofp_aggregate_stats_request()))

# global variables
holdFlows = {}
switchOrder = {}
switchCounter = int(0)
switchCounter2 = int(0)
result = []
readin = []
interimNormal = [] # global lists used to hold dynamically-changing values, 
interimRedundant = [] # then transfer them to normal and redundant when stable

#GLOBAL CONSTANTS WHICH NEED UPDATING IF YOU CHANGE TOPOLOGY:
#1) switch lists
#2) hosts IPs lists
#3) links lists
# as below.....

#1) switch lists

#tree4 switch lists
#mdf = [1,2] 
#idf = [3,4,5,6] 
#hwc = [7,8,9,10] 

#tree5 switch lists
mdf = [1] 
idf = [2,3,4,5] 
hwc = [6,7,8,9,10,11,12,13] 

#2) host IPs lists

#tree4 host IPs (mininet 2)
#hostIP = "10.0.0.1,10.0.0.2,10.0.0.3,10.0.0.4,10.0.0.5,10.0.0.6"

#tree5 host IPs (mininet 2)
hostIP = "10.0.0.1,10.0.0.2,10.0.0.3,10.0.0.4,10.0.0.5,10.0.0.6,10.0.0.7,10.0.0.8,10.0.0.9,10.0.0.10"

#3) links lists

# tree4 links lists for deterministic topology 
#normal = [[1,1,2,1],[1,2,3,1],[1,3,4,1],[1,4,5,1],[2,5,6,2],[3,3,7,1],[4,4,8,2],[5,3,9,1],[6,4,10,2]]
#redundant = [[1,5,6,1],[2,2,3,2],[2,3,4,2],[2,4,5,2],[3,4,8,1],[4,3,7,2],[5,4,10,1],[6,3,9,2]]

# tree5 links lists for deterministic topology
redundant = [[2,4,8,1],[2,5,9,1],[3,2,6,2],[3,3,7,2],[3,6,10,1],[4,2,9,3],[4,5,12,1],[4,6,13,1],[5,2,10,3],[5,3,11,2]]
normal = [[1,1,2,1],[1,2,3,1],[1,3,4,1],[1,4,5,1],[2,2,6,1],[2,3,7,1],[3,4,8,2],[3,5,9,2],[4,3,10,2],[4,4,11,1],[5,4,12,2],[5,5,13,2]]



pathLength = int(4) # this is the maximum number of switches traversed which consitute a path which is too short to optimise
flowDuration = int(10) # this is the minimum duration of a flow if it is to be considered for optimisation
flowBytes = int(16800) # this is the minimum byte-total of a flow it is to be considered for optimisation

mdflen = len(mdf)
idflen = len(idf)
hwclen = len(hwc)

toOpen = "/var/www/nodes"
writer = open(toOpen,'w') # open file...
writer.write("%s\n%s\n%s\n%s" %(mdflen,idflen,hwclen,hostIP))# write the nodes to the file
writer.close() # closing writer object


toOpen = "/home/mininet/pox/topoType"
deter = open(toOpen).read() # open file...
if deter == 'deterministic':
  Timer (5, detFirstThingsFirst)
  Timer (10, stpLaunch)
  Timer(12, launch, recurring = True)
else:
  Timer (5, nondetFirstThingsFirst)
  Timer(12, launch, recurring = True)