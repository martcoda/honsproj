
"""Custom topology example

author: Brandon Heller (brandonh@stanford.edu)

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import dumpNodeConnections

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add nodes
        cSwitch1 = self.addSwitch( 's1' )
        cSwitch2 = self.addSwitch( 's2' )
        dSwitch3 = self.addSwitch( 's3' )
        dSwitch4 = self.addSwitch( 's4' )
        dSwitch5 = self.addSwitch( 's5' )
        dSwitch6 = self.addSwitch( 's6' )
        aSwitch7 = self.addSwitch( 's7' )
        aSwitch8 = self.addSwitch( 's8' )
        aSwitch9 = self.addSwitch( 's9' )
        aSwitch10 = self.addSwitch( 's10' )
        host11 = self.addHost( 'h11' )
        host12 = self.addHost( 'h12' )
        host13 = self.addHost( 'h13' )
        host14 = self.addHost( 'h14' )
        host15 = self.addHost( 'h15' )
        host16 = self.addHost( 'h16' )

        optionalParams = {} #{'bw':10, 'delay':'10ms'}
        # , 'loss':0, 'max_queue_size':1000, 'use_htb':'True'

        # Add edges
        self.addLink( cSwitch1, cSwitch2, **optionalParams ) # s1p1 - s2p1
        self.addLink( cSwitch1, dSwitch3, **optionalParams ) # s1p2 - s3p1
        self.addLink( cSwitch1, dSwitch4, **optionalParams ) # s1p3 - s4p1
        self.addLink( cSwitch1, dSwitch5, **optionalParams ) # s1p4 - s5p1
        self.addLink( cSwitch1, dSwitch6, **optionalParams ) # s1p5 - s6p1 (redundant)

        self.addLink( cSwitch2, dSwitch3, **optionalParams ) # s2p2 - s3p2 (redundant)
        self.addLink( cSwitch2, dSwitch4, **optionalParams ) # s2p3 - s4p2 (redundant)
        self.addLink( cSwitch2, dSwitch5, **optionalParams ) # s2p4 - s5p2 (redundant)
        self.addLink( cSwitch2, dSwitch6, **optionalParams ) # s2p5 - s6p2

        self.addLink( dSwitch3, aSwitch7, **optionalParams ) # s3p3 - s7p1
        self.addLink( dSwitch3, aSwitch8, **optionalParams ) # s3p4 - s8p1 (redundant)

        self.addLink( dSwitch4, aSwitch7, **optionalParams ) # s4p3 - s7p2 (redundant)
        self.addLink( dSwitch4, aSwitch8, **optionalParams ) # s4p4 - s8p2

        self.addLink( dSwitch5, aSwitch9, **optionalParams ) # s5p3 - s9p1
        self.addLink( dSwitch5, aSwitch10, **optionalParams ) # s5p4 - s10p1 (redundant)

        self.addLink( dSwitch6, aSwitch9, **optionalParams ) # s6p3 - s9p2 (redundant)
        self.addLink( dSwitch6, aSwitch10, **optionalParams ) # s6p4 - s10p2

        self.addLink( aSwitch7, host11, **optionalParams ) # s7p3 - h11
        self.addLink( aSwitch7, host12, **optionalParams ) # s7p4 - h12

        self.addLink( aSwitch8, host13, **optionalParams ) # s8p3 - h13

        self.addLink( aSwitch9, host14, **optionalParams ) # s9p3 - h14

        self.addLink( aSwitch10, host15, **optionalParams ) # s10p3 - h15
        self.addLink( aSwitch10, host16, **optionalParams ) # s10p4 - h16


        # Links lists [[switch, port, switch, port]]
        #normal = [[1,1,2,1],[1,2,3,1],[1,3,4,1],[1,4,5,1],[2,5,6,2],[3,3,7,1],[4,4,8,2],[5,3,9,1],[6,4,10,2]]
        #redundant = [[1,5,6,1],[2,2,3,2],[2,3,4,2],[2,4,5,2],[3,4,8,1],[4,3,7,2],[5,4,10,1],[6,3,9,2]]



topos = { 'mytopo': ( lambda: MyTopo() ) }
