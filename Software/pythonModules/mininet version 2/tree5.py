
"""Custom topology example

author: Brandon Heller (brandonh@stanford.edu)

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import dumpNodeConnections

class MyTopo( Topo ):
    "Simple topology example."

    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add nodes
        cSwitch1 = self.addSwitch( 's1' )
        dSwitch2 = self.addSwitch( 's2' )
        dSwitch3 = self.addSwitch( 's3' )
        dSwitch4 = self.addSwitch( 's4' )
        dSwitch5 = self.addSwitch( 's5' )
        aSwitch6 = self.addSwitch( 's6' )
        aSwitch7 = self.addSwitch( 's7' )
        aSwitch8 = self.addSwitch( 's8' )
        aSwitch9 = self.addSwitch( 's9' )
        aSwitch10 = self.addSwitch( 's10' )
        aSwitch11 = self.addSwitch( 's11' )
        aSwitch12 = self.addSwitch( 's12' )
        aSwitch13 = self.addSwitch( 's13' )
        host1 = self.addHost( 'h1' )
        host2 = self.addHost( 'h2' )
        host3 = self.addHost( 'h3' )
        host4 = self.addHost( 'h4' )
        host5 = self.addHost( 'h5' )
        host6 = self.addHost( 'h6' )
        host7 = self.addHost( 'h7' )
        host8 = self.addHost( 'h8' )
        host9 = self.addHost( 'h9' )
        host10 = self.addHost( 'h10' )

        # Add edges
        self.addLink( cSwitch1, dSwitch2 ) 
        self.addLink( cSwitch1, dSwitch3 ) 
        self.addLink( cSwitch1, dSwitch4 ) 
        self.addLink( cSwitch1, dSwitch5 )
		
        self.addLink( dSwitch2, aSwitch6 )
        self.addLink( dSwitch2, aSwitch7 )
        self.addLink( dSwitch2, aSwitch8 )
        self.addLink( dSwitch2, aSwitch9 )
		
        self.addLink( dSwitch3, aSwitch6 )
        self.addLink( dSwitch3, aSwitch7 )
        self.addLink( dSwitch3, aSwitch8 )
        self.addLink( dSwitch3, aSwitch9 )
        self.addLink( dSwitch3, aSwitch10 )
		
        self.addLink( dSwitch4, aSwitch9 )
        self.addLink( dSwitch4, aSwitch10 )
        self.addLink( dSwitch4, aSwitch11 )
        self.addLink( dSwitch4, aSwitch12 )
        self.addLink( dSwitch4, aSwitch13 )
		
        self.addLink( dSwitch5, aSwitch10 )
        self.addLink( dSwitch5, aSwitch11 )
        self.addLink( dSwitch5, aSwitch12 )
        self.addLink( dSwitch5, aSwitch13 )

        self.addLink( aSwitch6, host1 )
        self.addLink( aSwitch7, host2 )
        self.addLink( aSwitch8, host3 )
        self.addLink( aSwitch8, host4 )
        self.addLink( aSwitch9, host5 )
        self.addLink( aSwitch10, host6 )
        self.addLink( aSwitch11, host7 )
        self.addLink( aSwitch11, host8 )
        self.addLink( aSwitch12, host9 )
        self.addLink( aSwitch13, host10 )

        # Links lists [[switch, port, switch, port]]

topos = { 'mytopo': ( lambda: MyTopo() ) }
